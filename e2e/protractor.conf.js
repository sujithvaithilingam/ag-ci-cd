// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts


const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: [ "--disable-gpu", "--headless" ]
      //args: [ "--headless",'--no-sandbox', '--disable-setuid-sandbox', "--disable-gpu", "--window-size=800,600", '--disable-dev-shm-usage', '--disable-software-rasterizer' ]
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

// const {SpecReporter} = require('jasmine-spec-reporter');
// process.env.LANG = 'en'; // set your browser language
// exports.config = {
//   allScriptsTimeout: 110000,
//   specs: [
//     './e2e/**/*.e2e-spec.ts',
//   ],
//   capabilities: {
//     'browserName': 'phantomjs',
//     'phantomjs.binary.path': require('phantomjs-prebuilt').path,
//     'phantomjs.cli.args': ['--remote-debugger-port=8081'],
//     'phantomjs.ghostdriver.cli.args': ['--loglevel=DEBUG'],
//   },
//   baseUrl: 'http://localhost:4200/',
//   framework: 'jasmine',
//   jasmineNodeOpts: {
//     showColors: true,
//     defaultTimeoutInterval: 50000,
//     print: function () {}
//   },
//   useAllAngular2AppRoots: true,
//   beforeLaunch: function () {
//     require('ts-node').register({
//       project: 'e2e/tsconfig.e2e.json',
//     });
//   },
//   onPrepare: function () {
//     jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
//   }
// };

