import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './guard/auth-service/auth.service';
import { GlobalServiceService } from './guard/global-service/global-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit   {
  title = 'remote-work';
  public islogin : boolean = false;

  constructor(
    private router : Router, 
    private authService : AuthService,
    public globalService : GlobalServiceService
    ){ }

  ngOnInit(){
    this.islogin = this.authService.IsLogedIn();
  }

}
