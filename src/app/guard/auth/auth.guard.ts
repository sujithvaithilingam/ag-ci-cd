import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../../login/login-service/login.service';
import { GlobalServiceService } from '../global-service/global-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router : Router,
    private loginService : LoginService, 
    private globalService : GlobalServiceService
    ){}
    
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      // debugger;
      let token : string = localStorage.getItem(this.loginService.LoginUserToken);


      if(token) {
        this.globalService.SetLocalValues();
        return true;
      }
      else {
        this.globalService.SetLocalValues();
        this.router.navigate(['/login'])
        return false;
      }
  }
}
