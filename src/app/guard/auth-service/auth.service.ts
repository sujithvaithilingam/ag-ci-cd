import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../../login/login-service/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient, private loginService : LoginService ) { }

  public GetUserCrendials(username: string, password: string) : Observable<any> {
    return this.http.get('')
  }
  public IsLogedIn() : boolean{
    let token : string = localStorage.getItem(this.loginService.LoginUserToken)
    if(token)
      return true;
    else
      return false;
  }
}
