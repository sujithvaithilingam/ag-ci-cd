import { Injectable } from '@angular/core';
import { LoginService } from '../../login/login-service/login.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalServiceService {

  constructor() { }

  public BaseUrl = "http://localhost:60584/"

  public globalScope = {
    userName : 'sujith',
    isLogedIn : false,
    UserToken : 'tokens'
  };

  public LoginStatus : string = 'LoginStatus';
  public LoginUserName : string = 'UserName';
  public LoginUserInfo : string = 'UserInfo';
  public LoginUserToken : string = 'UserToken';

  public SetValue(value : boolean){
    this.globalScope.isLogedIn = value;
  }

  public SetLocalValues(){
    this.globalScope.isLogedIn = localStorage.getItem(this.LoginStatus) == 'true' ? true : false;
    this.globalScope.userName = localStorage.getItem(this.LoginUserName);
    this.globalScope.UserToken = localStorage.getItem(this.LoginUserToken);
  }
}
