import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { GlobalServiceService } from '../../guard/global-service/global-service.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http : HttpClient,
    private globalService : GlobalServiceService
    ) { }

    public LoginStatus : string = 'LoginStatus';
    public LoginUserName : string = 'UserName';
    public LoginUserInfo : string = 'UserInfo';
    public LoginUserToken : string = 'UserToken';

  public UserLogin(username : string, passwod : string) : Observable<any>{
    return this.http.get(this.globalService.BaseUrl + 'api/Login?UserName='+ username +'&Password=' + passwod)
      .pipe(catchError(err=> this.ErrorHandler(err)));
  }

  ErrorHandler(value){
    return value;
  }
}
