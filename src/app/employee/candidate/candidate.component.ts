import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CandidateCellComponent } from './candidate-cell/candidate-cell.component';
import { CandidateCreatePopupComponent } from './candidate-create-popup/candidate-create-popup.component';


@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.scss']
})
export class CandidateComponent implements OnInit {

  constructor( 
    private http : HttpClient, 
    public snackBar: MatSnackBar, 
    private router : Router,
    private dialog : MatDialog
    ) { }
  
  ngOnInit( ) {
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };
  
  public rowData;
  public columnDefs = [
		{headerName: 'User Id ', field: 'userId' , cellRendererFramework : CandidateCellComponent, checkboxSelection : true},
		{headerName: 'Title ', field: 'title' },
    {headerName: 'completed', field: 'completed'},
    {headerName: '2nd title ', field: 'title' },
		{headerName: 'Title ', field: 'title' },
		{headerName: 'User Id ', field: 'userId' },
    {headerName: '4tile ', field: 'title' },
    {headerName: 'Title ', field: 'title' },
    {headerName: '6t ', field: 'title',  },
  ];
  
  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(d => this.rowData = d)
    console.log(params);
  }

  onSelectionChanged() : void {
    let selectedData : any = this.gridApi.getSelectedRows();
    if (selectedData) {
      debugger
      this.snackBar.open(selectedData[0].title, 'close', { duration : 700});
    }
  }

  public CreateEmployee() : void {
    const createemployee = this.dialog.open(CandidateCreatePopupComponent, {});

    createemployee.afterClosed().subscribe(
      result => console.log(result)
    );
  }

  public CandidateProcess(data) : void {
    // this.router.navigate(['/employee-details', data.RecId])
    this.router.navigate(['/candidate-process', 324])
  }

}
