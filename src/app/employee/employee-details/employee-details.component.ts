import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,  ParamMap, Router } from '@angular/router';


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {

  constructor(private router : Router, private route : ActivatedRoute) { }

  public RecId;

  ngOnInit() {
    // for get recid form url
    this.GetRecIdFromRouter();
  }

  public GetRecIdFromRouter() : void {
    this.route.paramMap
      .subscribe((params : ParamMap) => {
        let recid = parseInt(params.get('RecId'));
        this.RecId = recid;
      })
  }


}
