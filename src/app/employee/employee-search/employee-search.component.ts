import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.scss']
})
export class EmployeeSearchComponent implements OnInit {

  constructor(
    private router : Router
  ) { }
  
  public Employeename;
  ngOnInit() {
  }

  public GoToEmployeeDetails(RecId) : void {
    this.router.navigate(['employee-details', RecId])
  }
  public EmployeeSearch(event) : void {
    this.Employeename = '';
    console.log(event);
  }

}
