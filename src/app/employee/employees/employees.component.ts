import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { EmployeeCustomCellComponent } from '../tools/employee-custom-cell/employee-custom-cell.component';
import { Router } from '@angular/router';
import { EmployeeCreatePopupComponent } from '../tools/employee-create-popup/employee-create-popup.component';

import Swal from 'sweetalert2';




@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  constructor( 
    private http : HttpClient, 
    public snackBar: MatSnackBar, 
    private router : Router,
    private dialog : MatDialog
    ) { }
  
  ngOnInit( ) {
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };
  
  public rowData;
  public columnDefs = [
		{headerName: 'User Id ', field: 'userId' , cellRendererFramework : EmployeeCustomCellComponent, checkboxSelection : true},
		{headerName: 'Title ', field: 'title' },
    {headerName: 'completed', field: 'completed'},
    {headerName: '2nd title ', field: 'title' },
		{headerName: 'Title ', field: 'title' },
		{headerName: 'User Id ', field: 'userId' },
    {headerName: '4tile ', field: 'title' },
    {headerName: 'Title ', field: 'title' },
    {headerName: '6t ', field: 'title',  },
  ];
  
  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe(d => this.rowData = d)
    console.log(params);
    // Swal('Hello world!');
    // Swal({
    //   title: 'Are you sure?',
    //   text: 'You will not be able to recover this imaginary file!',
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: 'Yes, delete it!',
    //   cancelButtonText: 'No, keep it'
    // }).then((result) => {
    //   if (result.value) {
    //     Swal(
    //       'Deleted!',
    //       'Your imaginary file has been deleted.',
    //       'success'
    //     )
    //   // For more information about handling dismissals please visit
    //   // https://sweetalert2.github.io/#handling-dismissals
    //   } else if (result.dismiss === Swal.DismissReason.cancel) {
    //     Swal(
    //       'Cancelled',
    //       'Your imaginary file is safe :)',
    //       'error'
    //     )
    //   }
    // })
  }

  onSelectionChanged() : void {
    let selectedData : any = this.gridApi.getSelectedRows();
    if (selectedData) {
      debugger
      this.snackBar.open(selectedData[0].title, 'close', { duration : 700});
    }
    }

    public SelectedRow(int : number) : void {
      this.router.navigate(['/employee-details']);
    }
    
    public CreateEmployee() : void {
      const createemployee = this.dialog.open(EmployeeCreatePopupComponent, {});

      createemployee.afterClosed().subscribe(
        result => console.log(result)
      );
    }

    public EditEmployee() : void {
      const dialogRef = this.dialog.open(EmployeeCreatePopupComponent, { });

      dialogRef.afterClosed().subscribe(result => {
        debugger
        console.log(result)
      }); 
    }

    public DetailsEmployee(data) : void {
      // this.router.navigate(['/employee-details', data.RecId])
      this.router.navigate(['/employee-details', 324])
    }

}
