import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-custom-cell',
  templateUrl: './employee-custom-cell.component.html',
  styleUrls: ['./employee-custom-cell.component.scss']
})
export class EmployeeCustomCellComponent implements OnInit {
  public data;
  public paramsApi;
  constructor() { }
  agInit(params){
    this.paramsApi = params;
    this.data = params.value;
  }

  ngOnInit() {
  }

  public employeeDetails() : void {
    this.paramsApi.context.componentParent.DetailsEmployee(this.paramsApi.data);
  }

  public employeeEdit() : void {
    this.paramsApi.context.componentParent.CreateEmployee();
  }
}
