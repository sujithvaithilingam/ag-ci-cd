import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCustomCellComponent } from './employee-custom-cell.component';

describe('EmployeeCustomCellComponent', () => {
  let component: EmployeeCustomCellComponent;
  let fixture: ComponentFixture<EmployeeCustomCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeCustomCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCustomCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
