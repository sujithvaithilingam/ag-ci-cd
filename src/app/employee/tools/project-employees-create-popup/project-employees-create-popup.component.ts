import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-project-employees-create-popup',
  templateUrl: './project-employees-create-popup.component.html',
  styleUrls: ['./project-employees-create-popup.component.scss']
})
export class ProjectEmployeesCreatePopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ProjectEmployeesCreatePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any) {}

  ngOnInit() {
  }

  save(Task): void {
    this.dialogRef.close(Task);
  }
  
  close() : void {
    this.dialogRef.close();
  }
}
