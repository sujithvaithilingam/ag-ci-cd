import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-employee-create-popup',
  templateUrl: './employee-create-popup.component.html',
  styleUrls: ['./employee-create-popup.component.scss']
})
export class EmployeeCreatePopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EmployeeCreatePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any) {}

  ngOnInit() {

  }
  
  public save(data) : void{
    this.dialogRef.close(data);
  }

  public close() : void {
    this.dialogRef.close();
  }

}
