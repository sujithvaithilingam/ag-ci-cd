import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-details-all-tasks',
  templateUrl: './employee-details-all-tasks.component.html',
  styleUrls: ['./employee-details-all-tasks.component.scss']
})
export class EmployeeDetailsAllTasksComponent implements OnInit {

  constructor( 
    private http : HttpClient, 
    ) { }
  
  ngOnInit( ) {
    this.http.get('https://jsonplaceholder.typicode.com/todos')
      .subscribe(
        data => this.rowData = data
      );
  }

  public gridApi;
  public gridColumnApi;
  public currentSelectedData;
  public gridOptions = {
    context : {
      componentParent : this
    }
  };

  public rowData;


  public columnDefs = [
		{headerName: 'userId', field: 'userId' },
    {headerName: 'id ', field: 'id' }, 
    {headerName: 'title ', field: 'title' }, 
  	{headerName: 'completed ', field: 'completed' }, ]
}
