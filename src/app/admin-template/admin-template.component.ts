import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { AuthGuard } from '../guard/auth/auth.guard';
import { GlobalServiceService } from '../guard/global-service/global-service.service';


@Component({
  selector: 'app-admin-template',
  templateUrl: './admin-template.component.html',
  styleUrls: ['./admin-template.component.scss']
})
export class AdminTemplateComponent implements OnInit {
  public islogin = true;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  constructor(
     changeDetectorRef: ChangeDetectorRef,
     media: MediaMatcher, 
     private router : Router, 
     private auth : AuthGuard,
     public globalService : GlobalServiceService
     ) {

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit(){
    
  }

  LogOut() : void {
    localStorage.clear();
    this.globalService.SetLocalValues();
    this.router.navigate(['/home'])
  }
}
