import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, observable, } from 'rxjs';
import { catchError} from 'rxjs/operators'

 

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  BaseUri:string = "http://192.168.0.26/";
  constructor(private http : HttpClient) { }

  public GetCustomerList() {
    return this.http.get(this.BaseUri + "api/GetCustomerList");
  }
  
  ErrorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message || 'server error' )
  }

  public GetTaskList() : Observable<any>{
    return this.http.get(this.BaseUri + 'api/GetTaskList')
  } 

  public CreateNewTask(Task){    
    return this.http.post(this.BaseUri + 'api/CreateTask?Task='+ Task, null);
    // return this.http.post(this.BaseUri + 'api/CreateTask',Task, {
    //   headers : new  HttpHeaders({'Content-Type': 'application/json'})
    // });
  }

  public UpdateTask(Task){
    return this.http.post(this.BaseUri+ 'api/UpdateTask?Task='+ Task,null)
  }

  public EditTask(Id){
    return this.http.get(this.BaseUri + 'api/TaskEdit?Id='+Id)
  }
  public CustomerInsert(data){
    return this.http.post(this.BaseUri + 'api/CreateCustomer', data, {})
  }

  // public DeleteByTask(RecId){
  //   return this.http.po(this.BaseUri + 'api/DeleteByTask' + RecId, )
  // }

  public ChangeStatusForTask() {
    return this.http.get(this.BaseUri + 'api/ChangeStatustForTask')
  }

  public AddMultipleAssignmentForTask(){
    return this.http.get(this.BaseUri + 'api/AddMultipleAssigmentForTask');
  }  

  public ProjectDropDown(){
    return this.http.get(this.BaseUri + 'api/GetProjectDrop');
  }
  public ProjectEmployeeDrop(Id){
    return this.http.get(this.BaseUri + 'api/GetEmployeeforProjectDrop?Id='+ Id);
  }
  public GetLookupByTypeId(Id){
    return this.http.get(this.BaseUri + 'api/GetLookupByTypeId?Id='+ Id);
  }
  public GetTaskDetails(Id){
    return this.http.get(this.BaseUri + 'api/GetTaskDetailsVM?RecId='+ Id);
  }
  public GetTaskAssignHistory(Id){
    return this.http.get(this.BaseUri + 'api/GetTaskAssignment?Id='+ Id);
  }
  public GetTaskstatushistory(Id){
    return this.http.get(this.BaseUri + 'api/GetTaskStatusHistory?Id='+ Id);
  }

  
}
