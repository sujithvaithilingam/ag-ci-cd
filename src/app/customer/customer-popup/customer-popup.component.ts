import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-customer-popup',
  templateUrl: './customer-popup.component.html',
  styleUrls: ['./customer-popup.component.scss']
})
export class CustomerPopupComponent implements OnInit {

 
  constructor(
    public dialogRef: MatDialogRef<CustomerPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any) {}


  ngOnInit() {
  }

  save(Customer): void {
    this.dialogRef.close(Customer);
  }
  
  close() : void {
    this.dialogRef.close();
  }
}
