import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CustomerService } from '../customerservice/customer.service';


@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent implements OnInit {

  constructor(private router : Router,
     private route : ActivatedRoute,
     private customerservice:CustomerService
     ) { }

  public RecId;
  public Header;
  public AssignHistory;
  public CurrentActiveEmployees;
  public StatusHistory;

  ngOnInit() {
    // for get recid form url
    this.GetRecIdFromRouter();
    this.GetTakAssingmentHistory();
    this.GetTaskStatusHistory();
  }

  public GetRecIdFromRouter() : void {
    debugger;
    this.route.paramMap
      .subscribe((params : ParamMap) => {
        let recid = parseInt(params.get('RecId'));
        this.RecId = '' + recid;
        this.customerservice.GetTaskDetails(this.RecId)
        .subscribe((data : any) => {
          this.Header = data;
        })
      })
  }
  public GetTakAssingmentHistory():void{
    debugger;
    this.customerservice.GetTaskAssignHistory(this.RecId)
    .subscribe((data :any ) => {
      this.AssignHistory = data.History;
      this.CurrentActiveEmployees=data.CurrentActive;
    })
  }
  public GetTaskStatusHistory():void{
    debugger;
    this.customerservice.GetTaskstatushistory(this.RecId)
    .subscribe((data :any ) => {
      this.StatusHistory = data;      
    })
  }

  
}
