import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CustomerService } from '../../customerservice/customer.service';

@Component({
  selector: 'app-task-popup',
  templateUrl: './task-popup.component.html',
  styleUrls: ['./task-popup.component.scss']
})
export class TaskPopupComponent implements OnInit {

  constructor(
   private customerService : CustomerService,
    public dialogRef: MatDialogRef<TaskPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any) {}

   public projectdrop = [];
   public projectEmployeedrop = [];
   public PriorityDrop = [] 
   public GroupDrop=[];
    GetProjectDrop(){
      this.customerService.ProjectDropDown()
        .subscribe((data : any) => {
          this.projectdrop=data;
        })
    }

  ngOnInit() {
    this.GetProjectDrop();
    this.GetPriorityDrop();
    this.GetGroupDrop();
  }

  save(Task): void {
    this.dialogRef.close(Task);
  }
  
  close() : void {
    this.dialogRef.close();
  }

  Getprojectemployee(event){
    debugger;
    this.customerService.ProjectEmployeeDrop(event)
    .subscribe((data : any) => {
      this.projectEmployeedrop=data;
    })

  }
 
  GetPriorityDrop(){
    let Id: number = 6; 
      this.customerService.GetLookupByTypeId(Id)
        .subscribe((data : any) => {
          this.PriorityDrop=data;
        })
    }
    GetGroupDrop(){
      let Id: number = 4; 
        this.customerService.GetLookupByTypeId(Id)
          .subscribe((data : any) => {
            this.GroupDrop=data;
          })
      }
}
