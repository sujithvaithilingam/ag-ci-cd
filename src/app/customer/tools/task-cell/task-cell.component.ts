import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-task-cell',
  template: ` 
  <mat-icon (click)="EditTask()">edit</mat-icon> &nbsp;
  <mat-icon (click)="TaskDetails()">assignment</mat-icon>
  `,
  styles: [`
  
  `]
})
export class TaskCellComponent implements OnInit {

  public data;
  public paramsApi;
  constructor() { }

  agInit(params) {
    this.paramsApi = params;
    this.data = params.value;
  }

  ngOnInit() {

  }

  public TaskDetails() : void {
    this.paramsApi.context.componentParent.TaskDetails(this.paramsApi.data);
  }

  public EditTask() : void {
    this.paramsApi.context.componentParent.EditTask(this.paramsApi.data);
  } 

}
